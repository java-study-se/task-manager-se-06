package com.morozov.tm.service;

import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.repository.TaskRepository;
import com.morozov.tm.util.ConsoleHelper;
import com.morozov.tm.util.DataValidation;

import java.text.ParseException;
import java.util.List;


public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> getAllTaskByUserId(String userId) throws RepositoryEmptyException {
        List<Task> taskList = taskRepository.findAllByUserId(userId);
        DataValidation.checkEmptyRepository(taskList);
        return taskList;
    }

    public Task addTask(String userId, String taskName, String taskDescription, String dataStart, String dataEnd, String projectId) throws StringEmptyException, ParseException {
        DataValidation.checkEmptyString(taskName, taskDescription, dataStart, dataEnd, projectId);
        Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setStartDate(ConsoleHelper.formattedData(dataStart));
        task.setEndDate(ConsoleHelper.formattedData(dataEnd));
        task.setIdProject(projectId);
        task.setUserId(userId);
        taskRepository.persist(task);
        return task;
    }

    public boolean deleteTask(String userId, String id) throws StringEmptyException {
        boolean isDeleted = false;
        DataValidation.checkEmptyString(id);
        if (taskRepository.findOneByUserId(userId, id) != null) {
            taskRepository.remove(id);
            isDeleted = true;
        }
        return isDeleted;
    }

    public void updateTask(String userId, String id, String name, String description, String startDate, String endDate, String projectId) throws RepositoryEmptyException, StringEmptyException, ParseException {
        DataValidation.checkEmptyRepository(taskRepository.findAll());
        DataValidation.checkEmptyString(id, name, description, startDate, endDate, projectId);
        Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setStartDate(ConsoleHelper.formattedData(startDate));
        task.setEndDate(ConsoleHelper.formattedData(endDate));
        task.setIdProject(projectId);
        task.setUserId(userId);
        taskRepository.merge(id, task);
    }

    public List<Task> getAllTaskByProjectId(String userId, String projectId) throws StringEmptyException, RepositoryEmptyException {
        DataValidation.checkEmptyString(projectId);
        List<Task> resultTaskList = taskRepository.findAllByProjectIdUserId(userId, projectId);
        DataValidation.checkEmptyRepository(resultTaskList);
        return resultTaskList;
    }

    public void deleteAllTaskByProjectId(String userId, String projectId) {
        taskRepository.deleteAllTaskByProjectId(userId, projectId);
    }

    public void clearTaskList() {
        taskRepository.removeAll();
    }
}
