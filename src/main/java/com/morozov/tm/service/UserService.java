package com.morozov.tm.service;

import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.entity.User;
import com.morozov.tm.repository.UserRepository;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.DataValidation;
import com.morozov.tm.util.MD5Hash;

public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User loginUser(String login, String password) throws UserNotFoundException, StringEmptyException {
        DataValidation.checkEmptyString(login,password);
        User user = userRepository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        String typePasswordHash = MD5Hash.getHash(password);
        if (typePasswordHash.equals(user.getPasswordHash())) {
            return user;
        } else return null;
    }
    public void registryUser(String login, String password) throws UserExistException, StringEmptyException {
        DataValidation.checkEmptyString(login,password);
        if(userRepository.findOneByLogin(login)!= null) throw new UserExistException();
        User user = new User();
        user.setLogin(login);
        user.setRole(UserRoleEnum.USER);
        user.setPasswordHash(MD5Hash.getHash(password));
        userRepository.persist(user);
    }
    public void updateUserPassword(String id, String newPassword) throws StringEmptyException {
        User user = userRepository.findOne(id);
        DataValidation.checkEmptyString(newPassword);
        user.setPasswordHash(MD5Hash.getHash(newPassword));
        userRepository.merge(id, user);
    }
    public void updateUserProfile(String id, String newUserName, String newUserPassword) throws StringEmptyException, UserExistException {
        User user = userRepository.findOne(id);
        DataValidation.checkEmptyString(newUserName,newUserPassword);
        if (userRepository.findOneByLogin(newUserName) != null) throw new UserExistException();
        user.setLogin(newUserName);
        user.setPasswordHash(MD5Hash.getHash(newUserPassword));
        userRepository.merge(id,user);
    }
}
