package com.morozov.tm.service;

import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.repository.ProjectRepository;
import com.morozov.tm.util.ConsoleHelper;
import com.morozov.tm.util.DataValidation;

import java.text.ParseException;
import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<Project> getAllProject() throws RepositoryEmptyException {
        List<Project> projectList = projectRepository.findAll();
        DataValidation.checkEmptyRepository(projectList);
        return projectList;
    }

    public List<Project> getAllProjectByUserId(String userId) throws RepositoryEmptyException {
        List<Project> projectListByUserId = projectRepository.findAllByUserId(userId);
        DataValidation.checkEmptyRepository(projectListByUserId);
        return projectListByUserId;
    }


    public Project addProject(String userId, String projectName, String projectDescription, String dataStart, String dataEnd) throws StringEmptyException, ParseException {
        DataValidation.checkEmptyString(projectName, projectDescription, dataStart, dataEnd);
        Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setStartDate(ConsoleHelper.formattedData(dataStart));
        project.setEndDate(ConsoleHelper.formattedData(dataEnd));
        project.setUserId(userId);
        projectRepository.persist(project);
        return project;
    }

    public boolean deleteProject(String userId, String id) throws StringEmptyException {
        boolean isDeleted = false;
        DataValidation.checkEmptyString(id);
        if (projectRepository.findOneByUserId(userId, id) != null) {
            projectRepository.remove(id);
            isDeleted = true;
        }
        return isDeleted;
    }

    public void updateProject(String userId, String id, String projectName, String projectDescription, String dataStart, String dataEnd) throws RepositoryEmptyException, StringEmptyException, ParseException {
        DataValidation.checkEmptyRepository(projectRepository.findAllByUserId(userId));
        DataValidation.checkEmptyString(id, projectName, projectDescription, dataStart, dataEnd);
        Project project = new Project();
        project.setId(id);
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setStartDate(ConsoleHelper.formattedData(dataStart));
        project.setEndDate(ConsoleHelper.formattedData(dataEnd));
        project.setUserId(userId);
        projectRepository.merge(id, project);
    }

    public void clearProjectList() {
        projectRepository.removeAll();
    }

}
