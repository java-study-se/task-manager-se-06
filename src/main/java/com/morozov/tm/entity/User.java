package com.morozov.tm.entity;

import com.morozov.tm.security.role.UserRoleEnum;

import java.util.UUID;

public class User {
    private String login = "";
    private String passwordHash = "";
    private UserRoleEnum role = UserRoleEnum.USER;
    private String id = UUID.randomUUID().toString();

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public UserRoleEnum getRole() {
        return role;
    }

    public void setRole(UserRoleEnum role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
}
