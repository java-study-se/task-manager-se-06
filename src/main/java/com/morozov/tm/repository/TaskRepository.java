package com.morozov.tm.repository;

import com.morozov.tm.api.Repository;
import com.morozov.tm.entity.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository implements Repository<Task> {
    private Map<String, Task> taskMap = new HashMap<>();

    @Override
    public List<Task> findAll() {
        List<Task> taskList = new ArrayList<>();
        taskList.addAll(taskMap.values());
        return taskList;
    }

    public List<Task> findAllByProjectIdUserId(String userId, String projectId) {
        List<Task> resultListByUserId = new ArrayList<>();
        for (Task task : getAllTaskByProjectId(projectId)) {
            if (task.getUserId().equals(userId)) resultListByUserId.add(task);
        }
        return resultListByUserId;
    }

    public List<Task> findAllByUserId(String userId) {
        List<Task> resulеTaskList = new ArrayList<>();
        for (Task task : findAll()) {
            if (task.getUserId().equals(userId)) resulеTaskList.add(task);
        }
        return resulеTaskList;
    }

    public Task findOneByUserId(String userId, String id) {
        Task resultTask = null;
        for (Task task : findAllByUserId(userId)) {
            if (task.getId().equals(id)) resultTask = task;
        }
        return resultTask;
    }


    @Override
    public Task findOne(String id) {
        return taskMap.get(id);
    }

    @Override
    public void persist(Task writeEntity) {
        taskMap.put(writeEntity.getId(), writeEntity);
    }

    @Override
    public void merge(String id, Task updateEntity) {
        taskMap.put(id, updateEntity);
    }

    @Override
    public void remove(String id) {
        taskMap.remove(id);
    }

    @Override
    public void removeAll() {
        taskMap.clear();
    }

    public List<Task> getAllTaskByProjectId(String projectId) {
        List<Task> resultList = new ArrayList<>();
        for (Task task : taskMap.values()) {
            if (task.getIdProject().equals(projectId)) resultList.add(task);
        }
        return resultList;
    }

    public void deleteAllTaskByProjectId(String userId, String projectId) {
        List<Task> taskToDelete = findAllByProjectIdUserId(userId, projectId);
        for (Task task : taskToDelete) {
            remove(task.getId());
        }
    }

}
