package com.morozov.tm.repository;

import com.morozov.tm.entity.User;
import com.morozov.tm.security.role.UserRoleEnum;
import com.morozov.tm.util.MD5Hash;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepository {

    private Map<String, User> userMap = new HashMap<>();

    public UserRepository() {
        User admin = new User();
        admin.setLogin("admin");
        admin.setPasswordHash(MD5Hash.getHash("admin"));
        admin.setRole(UserRoleEnum.ADMIN);
        User user = new User();
        user.setLogin("user");
        user.setPasswordHash(MD5Hash.getHash("user"));
        user.setRole(UserRoleEnum.USER);
        userMap.put(admin.getId(), admin);
        userMap.put(user.getId(), user);
    }

    public List<User> findAll() {
        List<User> userList = new ArrayList<>();
        userList.addAll(userMap.values());
        return userList;
    }

    public User findOne(String id) {
        return userMap.get(id);
    }

    public User findOneByLogin(String login) {
        List<User> userList = findAll();
        User foundUser = null;
        for (User user : userList
        ) {
            if (user.getLogin().equals(login)) foundUser = user;
        }
        return foundUser;
    }

    public void persist(User user) {
        userMap.put(user.getId(), user);
    }

    public void merge(String id, User updateUser){
        userMap.put(id,updateUser);
    }
    public void remove(String id){
        userMap.remove(id);
    }


}
