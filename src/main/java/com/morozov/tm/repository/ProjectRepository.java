package com.morozov.tm.repository;

import com.morozov.tm.api.Repository;
import com.morozov.tm.entity.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository implements Repository<Project> {
    private Map<String, Project> projectMap = new HashMap<>();

    @Override
    public List<Project> findAll() {
        List<Project> projectList = new ArrayList<>();
        projectList.addAll(projectMap.values());
        return projectList;
    }

    @Override
    public Project findOne(String id) {
        return projectMap.get(id);
    }

    public List<Project> findAllByUserId(String userId){
        List<Project> projectListByUserID = new ArrayList<>();
        for (Project project: findAll()) {
            if(project.getUserId().equals(userId)) projectListByUserID.add(project);
        }
        return projectListByUserID;
    }

    public Project findOneByUserId(String userId, String id){
        Project resultProject = null;
        for (Project project : findAllByUserId(userId)) {
            if(project.getId().equals(id)) resultProject = project;
        }
        return resultProject;
    }

    @Override
    public void persist(Project writeEntity) {
        projectMap.put(writeEntity.getId(), writeEntity);
    }

    @Override
    public void merge(String id, Project updateEntity) {
        projectMap.put(id, updateEntity);
    }

    @Override
    public void remove(String id) {
        projectMap.remove(id);
    }

    @Override
    public void removeAll() {
        projectMap.clear();
    }

}
