package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project";
    }

    @Override
    public void execute() {
        User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Введите ID проекта для удаления");
        String idDeletedProject = ConsoleHelper.readString();
        try {
            if (serviceLocator.getProjectService().deleteProject(currentUser.getId(), idDeletedProject)) {
                ConsoleHelper.writeString("Проект с ID: " + idDeletedProject + " удален");
                ConsoleHelper.writeString("Удаление задач с ID проекта: " + idDeletedProject);
                serviceLocator.getTaskService().deleteAllTaskByProjectId(currentUser.getId(), idDeletedProject);
            } else {
                ConsoleHelper.writeString("Проекта с данным ID не существует");
            }
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("ID не может быт пустым");
        }
    }

    @Override
    public boolean checkProtectedStatus() {
        return true;
    }
}
