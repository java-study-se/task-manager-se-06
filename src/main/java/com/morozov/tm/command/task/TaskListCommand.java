package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

import java.util.List;

public class TaskListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() {
        User currentUser = serviceLocator.getCurrentUser();
        try {
            List<Task> taskList = serviceLocator.getTaskService().getAllTaskByUserId(currentUser.getId());
            ConsoleHelper.writeString("Список задач:");
            for (int i = 0; i < taskList.size(); i++) {
                ConsoleHelper.writeString(String.format(("%d: %s"), i, taskList.get(i).toString()));
            }
        } catch (RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список задач пуст");
        }
    }

    @Override
    public boolean checkProtectedStatus() {
        return true;
    }
}
