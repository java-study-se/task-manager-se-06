package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

import java.text.ParseException;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create task";
    }

    @Override
    public void execute() {
        User currenetUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Введите имя новой задачи");
        String taskName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите описание новой задачи");
        String taskDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату начала новой задачи в формате DD.MM.YYYY");
        String startTaskDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату окончания новой задачи в формате DD.MM.YYYY");
        String endTaskDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите ID проекта задачи");
        String projectId = ConsoleHelper.readString();
        try {
            Task task = serviceLocator.getTaskService().addTask(((User) currenetUser).getId(), taskName, taskDescription, startTaskDate, endTaskDate, projectId);
            ConsoleHelper.writeString("Добавлена задача " + task.toString());
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (ParseException e) {
            ConsoleHelper.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }

    @Override
    public boolean checkProtectedStatus() {
        return true;
    }
}
