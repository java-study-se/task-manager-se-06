package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task";
    }

    @Override
    public void execute() {
        User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Введите порядковый номер задачи для удаления");
        String idDeletedTask = ConsoleHelper.readString();
        try {
            if (serviceLocator.getTaskService().deleteTask(currentUser.getId(), idDeletedTask)) {
                ConsoleHelper.writeString("Задача с порядковым номером " + idDeletedTask + " удален");
            }
            ConsoleHelper.writeString("Задачи с данным ID не существует");
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        }
    }

    @Override
    public boolean checkProtectedStatus() {
        return true;
    }
}
