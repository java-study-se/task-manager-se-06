package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.util.ConsoleHelper;

public class TaskClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        serviceLocator.getTaskService().clearTaskList();
        ConsoleHelper.writeString("Список задач очищен");
    }

    @Override
    public boolean checkProtectedStatus() {
        return true;
    }
}
