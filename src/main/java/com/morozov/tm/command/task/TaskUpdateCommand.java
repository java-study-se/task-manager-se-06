package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

import java.text.ParseException;

public class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-update";
    }

    @Override
    public String getDescription() {
        return "Update selected task";
    }

    @Override
    public void execute() {
        User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString("Введите ID задачи для изменения");
        String updateTaskID = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новое имя задачи");
        String updateTaskName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новое описание задачи");
        String updateTaskDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новую дату начала задачи в формате DD.MM.YYYY");
        String startUpdateTaskDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новую дату окончания задачи в формате DD.MM.YYYY");
        String endUpdateTaskDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите ID проекта задачи");
        String updateTaskProjectId = ConsoleHelper.readString();
        try {
            serviceLocator.getTaskService().updateTask(currentUser.getId(), updateTaskID, updateTaskName, updateTaskDescription, startUpdateTaskDate, endUpdateTaskDate, updateTaskProjectId);
            ConsoleHelper.writeString("Задача с порядковым номером " + updateTaskID + " обновлена");
        } catch (RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список задач пуст");
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (ParseException e) {
            ConsoleHelper.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }

    @Override
    public boolean checkProtectedStatus() {
        return true;
    }
}
