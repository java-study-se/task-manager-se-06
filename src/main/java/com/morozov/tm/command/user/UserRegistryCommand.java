package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.util.ConsoleHelper;

public class UserRegistryCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-reg";
    }

    @Override
    public String getDescription() {
        return "Registry new user";
    }

    @Override
    public void execute() {
        ConsoleHelper.writeString("Введите имя пользователя");
        String login = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите пароль пользователя");
        String password = ConsoleHelper.readString();
        try {
            serviceLocator.getUserService().registryUser(login,password);
            ConsoleHelper.writeString("Пользователь с логином " + login + " зарегистрирован");
        } catch (UserExistException e) {
            ConsoleHelper.writeString("Такой пользователь уже существует");
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        }
    }

    @Override
    public boolean checkProtectedStatus() {
        return false;
    }
}
