package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.User;
import com.morozov.tm.util.ConsoleHelper;

public class UserLogoutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Close current user session";
    }

    @Override
    public void execute() {
        User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelper.writeString(String.format("Сессия пользователя %s закрыта", currentUser.getLogin()));
        serviceLocator.setCurrentUser(null);
    }

    @Override
    public boolean checkProtectedStatus() {
        return true;
    }
}
